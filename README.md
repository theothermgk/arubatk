# Arubatk
`Version 0.02`

<div align="center">
![Arubatk 0.02 - Main Menu](https://gitlab.com/theothermgk/theothermgk.gitlab.io/raw/master/images/arubatk_002.png)
</div>
  
---  
  
This script was made to help technicians test the IDS systems of their Access Points. 

For now, it can be used to test against basic attacks like AP Flooding, Omerta Attacks, Rogue APs. I have included a list of IDS plugins as reference for building upon this script since in it's current state it does not cover most plugins. (Granted a lot are obsolete, like WEP & WPA attack detection). 

I hope to expand it's functionality in the near future. 

You can send me comments and suggestions on Twitter [@TheOtherMGK](https://twitter.com/@theothermgk)

# Pre-Installation
If you are not using Kali Linux, you might have to install these packages manually:

asleap - https://github.com/joswr1ght/asleap  
hostapd-wpe - https://github.com/OpenSecurityResearch/hostapd-wpe  
MDK4 - https://github.com/aircrack-ng/mdk4  

The rest should be available in most distributions of GNU/Linux.

# Installation

```
git clone https://gitlab.com/theothermgk/arubatk.git
cd arubatk
sudo ./arubatk.sh install
```

NOT RECOMMENDED: You can clone the **dev** branch instead for the latest development changes:
```
git clone --single-branch --branch dev https://gitlab.com/theothermgk/arubatk.git
```

# Usage

```
sudo ./arubatk.sh
```

# Development Notes

- The RADIUS options are not implemented yet.
- The mac OUI commands, etc are not implemented yet.
- Better automated options coming soon, i.e.: Setting the target MAC by SSID scan, etc.

# Guide

<div align="center">
![Arubatk - Editing the script](https://gitlab.com/theothermgk/theothermgk.gitlab.io/raw/master/images/arubatk_002_editing_script.png)
</div>

You can set defaults by editing the script:
- `$editor`, your favorite editor (Default: vim)  
- `$explorer`, your file browser (Default: nautilus)  
- `$dev`, for your attack interface  (Default: wlan0)  
- `$target`, the AP's MAC  
- `$client`, the MAC of a client of the $target/AP.  
- `$ip`, only used for nmap scans for now.  
- `$config_dir`, working directory of Arubatk (Default: /root/.config/arubatk)  

---

<div align="center">
![Arubatk - Editing temp vars](https://gitlab.com/theothermgk/theothermgk.gitlab.io/raw/master/images/arubatk_002_editing_temp_vars.png)
</div>

You can make temporary changes to the variables :
```
dev [interface name]
target [AP's MAC]
client [Client's MAC]
ip [network or client IP]
```

You can save or load the temporary variables using:
```
write
write [path/to/file]
load
load [path/to/file]
```

There are two menus for now The **Main Menu** and the **MITM Menu**: 
```
`p` Toggle between the Main Menu's Page 1 and 2.
`0` Toggle between the Main Menu and the MITM Menu.
```

Nearly all operations will open in an **xterm** terminal to free up your prefered terminal and scale up certain attacks.

# Examples

### Example 1 - Triggering the IDS
Use `as` to set the $target MAC and $client MAC. Then use `write` to save them to disk so that you may use `load` to restore them later. Otherwise, you can also edit **arubatk.sh** if you wish to set them as defaults.
You can now use `2` to perform an **Omerta Attack** on $client. You should see it show up immediately in your IDS log.

### Example 2 - Issues with Attacks
After launching an Aireplay attack, if you get no output, it could be because your interface is set to a bad channel. Use `channel [number]` to change the interface's channel and test the attack again. If the channel is showing "n/a", it's because it's not enable yet. Make sure you are in monitor mode and not managed.

# All Commands

### **Main Menu**  

 `0`: MITM Menu  
 `1`: MDK4 Deauth all on $target  
 `2`: Aireplay Deauth $client from $target  
 `3`: Aireplay Deauth all  
 `5`: MDK4 - Beacon Flood on $target  
 `6`: MDK4 - EAPOL Auth Flood  
  
 `m`: Start monitor mode on $dev  
`mm`: Stop monitor mode on $dev  
  
 `s`: Perform a quick airodump-ng scan  
`sd`: Perform a quick airodump-ng scan, and dump the output to $config_dir (/root/.config/arubatk/)  
  
 `n`: Nmap complete network map dump of $ip  
`ns`: Nmap host discorery on $ip nmap -sn  
 
 `c`: Clean changes made to system by Arubatk (Removes dumps, virtual interfaces, etc)  
  
 `v`: View $config_dir in $explorer (/root/.config/arubatk/)  
 `h`: View Main Menu help  
 `p`: View Page 2  
 
`as`: Run a "Setup Wizard" to set the $target and $client automatically.  

`help [command]`: Show additional information on [command]
 
`write`: Write the temporary variables to $config_dir/arubatk_vars.conf  
`write [path/to/file]`: Write the temporary vars to [path/to/file]  
 
`load`: Load the temporary variables from $config_dir/arubatk_vars.conf  
`load [path/to/file]`: Load the temporary vars from [path/to/file]  


### **MITM Menu**

`0`: Main Menu  
`1`: Start the hostapd-wpe rogue AP  
 
`c`: Create or Reset the $config_dir/hostpad-wpe.conf  
`s`: Run a "Setup Wizard" to set the hostapd $interface, $channel, $ssid & WPA2 $password  
`e`: Open the $config_dir/hostapd-wpe.conf in $editor  

`l`: Open the hostapd-wpe log in xterm  

`a`: Run asleap wizard for password cracking  

`v`: View $config_dir in $explorer (/root/.config/arubatk/)  
`h`: View MITM help  

# Credits

Special thanks to Musket Teams and the people behind Backtrack/Kali. Without them, I wouldn't be writing scripts today.
